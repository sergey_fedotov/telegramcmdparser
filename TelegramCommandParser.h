#pragma once
#include <UniversalTelegramBot.h>


#ifndef DEBUG_PRINT_
#define DEBUG_PRINT_(x)
#endif

struct TelegramHandler {
  
  typedef void( *CallbackFunc ) (const char * args, const TelegramMessage& msg);
//private:
  PGM_P token;
  CallbackFunc callback;
  const TelegramHandler * handlers; 

  // если определить инициализаторы, то нельзя инициализировать так в PROGMEM !!!!!!!
  // а нам надо !!!
  // TelegramHandler(PGM_P token= nullptr, CallbackFunc callback = nullptr):
  //   token(token), callback(callback)
  // {};

  // TelegramHandler( ):
  //   token(nullptr),callback(nullptr)
  // {};

//public:
 // metods
  bool equals(const char * str) const {
    int charIndex = 0;
    while( token[charIndex] != '\0'){
      const char c = str[charIndex];
      if ( token[charIndex] != c ) {
        return false;    
      }
      charIndex++; 
    }
    // начало совпало, а дальше должен быть конец или разделитель
    const char c = str[charIndex];
    if ( c == '\0' || c == ' ' || c == '_' ) return true;
    return false;
  };

  const char * getToken() const {
    return token;
  };
  inline int tokenLength() const {
    return token ? strlen( token) : 0;
  };
  inline bool isEmpty() const { return token == nullptr; };
  inline bool hasHandlers() const { return handlers != nullptr; };
  inline bool hasCallback() const { return callback != nullptr; };
  inline const TelegramHandler* getHandlers() const { return handlers; };
  inline CallbackFunc getCallback() const { return callback; };
  inline bool hasHandler() const { return callback != nullptr || handlers !=nullptr; };
};


class TelegramParser {
  private:
  const TelegramHandler * parserDatas;
  enum Index { NotFound = -1, IndexFound =0 };
  //static const int NotFound = -1;

  public:
  TelegramParser( const TelegramHandler * parserDatas):
     parserDatas(parserDatas)
  {
  };
  inline bool indexFound(const int index) const { return index > Index::NotFound; };
  inline bool indexNotFound(const int index) const { return !indexFound(index); }
  // bool found() const {
  //   static int rootIndex = Index::NotFound;
  //   bool return_val;

  //   if ( indexFound(rootIndex) ) {
  //     rootIndex = tokenIndex() ;    
  //     DEBUG_PRINT_("Root found. Parse args");
  //     DEBUG_PRINT_( rootIndex );
  //     return_val = indexFound(rootIndex);  
  //   } else {
  //     DEBUG_PRINT_("Parse root");
  //     return_val = indexFound(rootIndex);
  //     rootIndex = Index::NotFound;
  //   }
  //   DEBUG_PRINT_( return_val );
  //   return return_val;
  // };
  inline bool found() const {
    return tokenIndex() > Index::NotFound;
  };

  //unknown token or invalid arguments virtual handler
  virtual bool unknownCommand( ) const {
    return !found();    
  };

  const char * trim(const char * str) const {
    int i = 0;
    char c = str[i];
    while ( c != '\0') {
      if ( c == ' ' || c == '_' || c == '\t' ) {
        i++;
        c = str[i];
      } else break;
    } 
    return (const char *)&str[i];
  };

  
  int tokenIndex(const char * str = nullptr) const { 
    //DEBUG_PRINT_( parserDatas->tokenLength() ); 
    static int index = Index::NotFound;

    if ( str == nullptr ) {
      return index;
    }

    index = Index::NotFound;
    int i = Index::NotFound;
    while ( ! parserDatas[++i].isEmpty() ){
      DEBUG_PRINT_( (String("Compare '") + parserDatas[i].getToken() + "' with arg '" + str ) );
      //DEBUG_PRINT_( (String("str: ")+ str) );
      if ( parserDatas[i].equals( str ) ) {
        index = i;
        break;
      }
    }
    if ( indexFound(index)) { 
      DEBUG_PRINT_( String("found index ") + index ); 
    } else { 
      DEBUG_PRINT_("not found"); 
    }
    return index;
  };
  // const char * getArgs(const TelegramMessage& msg, const int index, const char * args = nullptr) const {
  //   const char * textArgs = args == nullptr ? 
  //     msg.text.c_str() :
  //     args;
  //   int argOffset = indexFound(index) ? parserDatas[index].tokenLength() : 0;
  //   return trim(textArgs + argOffset);
  // };


  // int eval(const TelegramMessage& msg, const char * args = nullptr) const {
  //   // const char * textArgs = args == nullptr ? 
  //   //     msg.text.c_str() :
  //   //     args;

  //   const char* textArgs = getArgs(msg, Index::NotFound, args);
              
  //   int index = tokenIndex(textArgs);  
  //   if ( indexFound(index) ) {        
  //     const TelegramHandler& parser = parserDatas[index];
  //     assert( (parser.hasHandler()) || F("Need at least one callback or handler"));

  //     DEBUG_PRINT_( parser.getToken() );
  //     //const char * restArgs = trim(textArgs + parser.tokenLength());
  //     const char * restArgs = getArgs(msg, index, args);

  //     int subIndex = Index::NotFound;
  //     if ( (restArgs[0] != '\0' && parser.hasHandlers() ) ||
  //       !parser.hasCallback() ) {
  //       TelegramParser subParser(parser.getHandlers() );
  //       subIndex = subParser.eval( msg, restArgs );   
  //     } 

  //     if ( indexNotFound(subIndex) && parser.hasCallback() ) { 
  //       DEBUG_PRINT_( String("Callback args ") + restArgs );
  //       parser.getCallback()( msg, restArgs ); 
  //     } else 
  //         index = subIndex; //(subIndex*256) + index;
  //   }
  //   return index;
  // };
  // 446837/31016

  
  const char * getArgs( const int index, const char * args ) const {
    int argOffset = 0;
    if ( indexFound(index) ) 
        argOffset = parserDatas[index].tokenLength();
    return trim(args + argOffset);
  };

  int eval(const char * args, const TelegramMessage& msg) const {
          
    int index = tokenIndex(args);  
    if ( indexFound(index) ) {        
      const TelegramHandler& parser = parserDatas[index];
      assert( (parser.hasHandler()) || F("Need at least one callback or handler"));

      DEBUG_PRINT_( parser.getToken() );
      const char * restArgs = getArgs( index, args);

      int subIndex = Index::NotFound;
      if ( (restArgs[0] != '\0' || !parser.hasCallback() )
          && parser.hasHandlers() ) {
        TelegramParser subParser(parser.getHandlers() );
        subIndex = subParser.eval( restArgs, msg );   
      } 

      if ( indexNotFound(subIndex) ) {
        if ( parser.hasCallback() ) { 
          DEBUG_PRINT_( String("Callback args ") + restArgs );
          parser.getCallback()( restArgs, msg ); 
        } else {
          DEBUG_PRINT_("Index = subIndex");
          index = subIndex; //(subIndex*256) + index;
        }
      }
    }
    return index;
  };
  inline int eval( const String& args, const TelegramMessage& msg ) const {
    return eval( args.c_str(), msg);
  }; 
  inline int eval( const TelegramMessage& msg) const {
    return eval( msg.text.c_str(), msg); // 446805 -> 446789
  };
};


