/*******************************************************************
    An example of how to use a custom reply keyboard markup.


     written by Vadim Sinitski (modified by Brian Lough)
 *******************************************************************/
#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <UniversalTelegramBot.h>

#define RELAY_TIMEOUT 5


#define STRINGIFY(s) SUB_STRING(s)
#define SUB_STRING(s) #s

#define DEBUG_PRINT { Serial.print('[');Serial.print(__LINE__); Serial.print(']'); Serial.println( __PRETTY_FUNCTION__); }
//#define DEBUG_PRINT_(x) { Serial.print((x)); Serial.print(' '); DEBUG_PRINT; }

//#define DEBUG_PRINT
//#define DEBUG_PRINT_

#include "Relay.h"
#include "Timer.h"
#include "mySHT.h"
#include "TelegramCommandParser.h"
#include "handlers.h"
#include "files.h"

// template <typename T> inline String& operator << (String &s, T n){
//   s += n; return s;
// };

// Wifi network station credentials
#define WIFI_SSID "my_ssid"
#define WIFI_PASSWORD "my_pass"
// Telegram BOT Token (Get from Botfather)
#define BOT_TOKEN "my_token"

const unsigned long BOT_MTBS = 5000; // mean time between scan messages
const unsigned long relay_timeout = RELAY_TIMEOUT * 1000;

MySHT tSensor(30000);
//X509List cert(TELEGRAM_CERTIFICATE_ROOT);
WiFiClientSecure secured_client;
UniversalTelegramBot bot(BOT_TOKEN, secured_client);
unsigned long bot_lasttime;          // last time messages' scan has been done

Relay relay(LED_BUILTIN, true ); //реле на пине встроеного светика, инвертированое управление, таймер на X сек
RelayTimer timer( &relay, relay_timeout );


MyParser telegramCmdParser( handlersRootT );
MyParser telegramCallBackQueryParser( handlersCallBackQueryT);
//MyParser helpParser( handlersHelp );
MyParser telegramChatActionParser( handlersChatActions );  

// void handleNewMessages(int numNewMessages)
// {
 
//   for (int i = 0; i < numNewMessages; i++)
//   {
//   DEBUG_PRINT_( numNewMessages );
//     //int index;
//     // Inline buttons with callbacks when pressed will raise a callback_query message
    
//     TelegramMessage& msg = bot.messages[i]; 
//     if (msg.type == "callback_query")
//     {
      
//       DEBUG_PRINT_( (String("Call back button pressed by: ") + 
//         msg.from.id + 
//         "\nData on the button: " +
//         msg.text )
//         );

//       // выполняем команду из очереди callback Telegram
//       /*index = */telegramCallBackQueryParser.eval( msg );
      
//       // отладочный вывод, если не получилось выполнить
//       if ( telegramCallBackQueryParser.unknownCommand() ) {
//         DEBUG_PRINT_( "Error parsing command" );
//       }
            
//     }
//     else
//     {
//       // String chat_id = msg.chat.id;

//       if ( msg.text.isEmpty() ){

//         // приглашения/удаление в/из группы/каналы для этого бота
//         if ( msg.chat.user_id.equals( bot.getId() ) ) {
//           // выполняем команду из member_status
//           telegramChatActionParser.eval(msg.chat.member_status, msg );
//           // если не получилось обработать, то и наплевать, может там пусто было или что-то что я не
//         }

//       } else {
//         // выполняем команду из msg.text
//         int index = telegramCmdParser.eval(msg );
//         DEBUG_PRINT_( telegramCmdParser.cmdIndex() );
//         DEBUG_PRINT_( index );
        
//         // если не получилось обработать, то сообщаем в чат "неизвестная команда"
//         if ( telegramCmdParser.indexNotFound(index) ){
//             telegramCmdParser.unknownCommand( &msg );
//         } 
       
//       }
//     } 
//   } //for
// //  DEBUG_PRINT;
// }

void setup()
{
  Serial.begin(115200);
  
  tSensor.begin();
  initFS();
  
  
  pinMode(LED_BUILTIN, OUTPUT); // initialize digital ledPin as an output.
  //delay(10);
  digitalWrite(LED_BUILTIN, HIGH);

  // Set WiFi to station mode and disconnect from an AP if it was Previously connected
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);

  // attempt to connect to Wifi network:
  Serial.print("Connecting to Wifi SSID ");
  Serial.print(WIFI_SSID);
  Serial.print(" ");
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  //secured_client.setTrustAnchors(&cert); // Add root certificate for api.telegram.org
  //secured_client.setInsecure(); 
  secured_client.setFingerprint( TELEGRAM_FINGERPRINT );

 
  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(500);
  }
  Serial.println();

  Serial.print("WiFi connected. IP address: ");
  Serial.println(WiFi.localIP());

  configTime(3, 0, "pool.ntp.org");
  Serial.print("Sync time ");
  time_t now = time(nullptr);
  while (now < 24 * 3600) { Serial.print(".");delay(100); now = time(nullptr); }

  Serial.println("");
  struct tm timeinfo;
  gmtime_r(&now, &timeinfo);
  Serial.print("Current time: ");
  Serial.print(asctime(&timeinfo));

  tSensor.autoDetect();
  if ( tSensor.isPresent() ){
    String f("Found ");
    switch( tSensor.getModel() ){
      case 0:
      f += "DHT22";
      break;

      case 1:
      f += "DHT11";
      break;

      case 2:
      f += "SHT21"; 
      break;

      default:
      f = "No sensor found"; 
    }
    //f += == 0 ? 22 : 11;
    Serial.println( f );
  }
}


//
//void switchRelay(int c=0, char **v=nullptr){
//    DEBUG_PRINT;
//    relay.switchOn();
//    Serial.println( "Timer switch");
//    ( {}, "Timer"); 
//};

  
void loop()
{
  static bool alarmed = false;

  if ( timer.loop() ){
    DEBUG_PRINT_( "Timer OFF & switch Relay");
    String t("Timer");
    if ( tSensor.isPresent() ){
      tSensor.parametersChanged();
      t << ". T=" << tSensor.getLastTemperature();
    }
    sendControlKeyboard( {}, t );
  }

  
  if ( /*timer.lastMs() > 3000*/ !timer.protectedPeriod(3000) && millis() - bot_lasttime > BOT_MTBS)
  {
    DEBUG_PRINT_( ( millis()-bot_lasttime ) );

    bot_lasttime = millis();
    
    int numNewMessages = bot.getUpdates(bot.nextMessageId());
    
    DEBUG_PRINT_(  ( "Telegram checked messages" + (numNewMessages > 0 ? ( String(" = ") + numNewMessages ) : String() ) ));
    while (numNewMessages > 0 ) // = bot.getUpdates(bot.nextMessageId()) )
    {
      DEBUG_PRINT_("got response");
      for (int i = 0; i < numNewMessages; i++)
      {
      DEBUG_PRINT_( numNewMessages );
        //int index;
        // Inline buttons with callbacks when pressed will raise a callback_query message
        
        TelegramMessage& msg = bot.messages[i]; 
        if (msg.type == "callback_query")
        {
          
          DEBUG_PRINT_( (String("Call back button pressed by: ") + 
            msg.from.id + 
            "\nData on the button: " +
            msg.text )
            );

          // выполняем команду из очереди callback Telegram
          /*index = */telegramCallBackQueryParser.eval( msg );
          
          // отладочный вывод, если не получилось выполнить
          if ( telegramCallBackQueryParser.unknownCommand() ) {
            DEBUG_PRINT_( "Error parsing command" );
          }
                
        }
        else
        {
          // String chat_id = msg.chat.id;

          if ( msg.text.isEmpty() ){

            // приглашения/удаление в/из группы/каналы для этого бота
            if ( msg.chat.user_id.equals( bot.getId() ) ) {
              // выполняем команду из member_status
              telegramChatActionParser.eval(msg.chat.member_status, msg );
              // если не получилось обработать, то и наплевать, может там пусто было или что-то что я не знаю
            }

          } else {
            // выполняем команду из msg.text
            int index = telegramCmdParser.eval( msg );
            DEBUG_PRINT_( telegramCmdParser.cmdIndex() );
            DEBUG_PRINT_( index );
            
            // если не получилось обработать, то сообщаем в чат "неизвестная команда"
            if ( telegramCmdParser.indexNotFound(index) ){
                telegramCmdParser.unknownCommand( &msg );
            } 
           
          }
        } 
      } //for
      numNewMessages = bot.getUpdates(bot.nextMessageId());
    } 
  }
  
  if ( tSensor.isPresent() ) 
    if  ( tSensor.onChange(MySHT::Temperature) && !alarmed ){
      if ( tSensor.getLastTemperature() > 24 ) {
        String s("Alarm!! Temp = ");
        s += tSensor.getLastTemperature();
        alarmed = sendControlKeyboard({}, s);
        
      } else if ( alarmed ){
        sendControlKeyboard({}, "Temp normal");
        alarmed = ! alarmed;
      }
      // fake request for WiFi stable
      //bot.getUpdates(bot.nextMessageId());
    }
}
