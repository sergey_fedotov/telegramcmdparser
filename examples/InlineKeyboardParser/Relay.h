#pragma once

#ifndef DEBUG_PRINT_
#define DEBUG_PRINT_(x)
#endif

#define MAX_COMMAND_ARG_SIZE 3

class Relay {
  private:
//  unsigned long startTimerMs = 0;
//  unsigned long timerMs;
  const uint8_t pin; 
  const bool inverse;
//  bool timerStatus = false;
  
//  union Argument {
//    double asDouble;
//    uint64_t asUInt64;
//    int64_t asInt64;
//    int asInt;
//    uint asUInt;
//    char asString[MAX_COMMAND_ARG_SIZE + 1];
//  };
//  void switchOn_(int c=0, char * v= nullptr){
//    this->switchOn();
//  };
    
  public:
  Relay(const uint8_t pin = 0, const bool inverse = false /*, unsigned long mtbs = 3000*/):
     /*timerMs(mtbs),*/ pin(pin), inverse(inverse)
  {
    pinMode(pin, OUTPUT);
    setOn();
//    timerStatus = false;
  };
  ~Relay(){};
  inline bool status() const {
    bool st = digitalRead(pin);
    return inverse ? !st : st;
  };
  inline bool setOn() const {
    digitalWrite(pin, !inverse);
    return status();
  };
  inline bool setOff() const {
    digitalWrite(pin, inverse);
    return status();
  };
  bool switchOn() const {
    DEBUG_PRINT_("");
    digitalWrite(pin, !digitalRead(pin));
    return status();
  };
  /*
  void setTimer(bool start = true){
    if ( start ) {
      DEBUG_PRINT;
      startTimerMs = millis();
    } 
    timerStatus = start;
  };
  void setTimer(unsigned long mtbs, bool start = false){
    timerMs = mtbs;
    setTimer(start);
    //startTimerMs = start ? millis() : 0;
  };
  void inline resetTimer(){
    setTimer(false);
  };

  bool restMoreTime(unsigned long period){
    if ( ! timerStatus ) return true;
    return millis() - startTimerMs < timerMs - period; 
  };
  
//  bool pollingTimer(){
//    bool ret = false;
//    if ( !timerStatus ) return ret;
//    
//    DEBUG_PRINT;
//    if ( millis() - startTimerMs > timerMs ){
//      switchOn();
//      ret = true;
//      resetTimer();
//    }  
//    return ret;
//  };

  virtual void onTimer( ) { 
    DEBUG_PRINT;
    this->switchOn(); 
    };
  
  bool pollingTimer(void (*func)(int argc, char **argv ) = nullptr, int argc=0, char **argv=nullptr ){ //union Argument *args, char *response) {
    bool ret = false;
    if ( !timerStatus ) return ret;
    
    DEBUG_PRINT;
    if ( millis() - startTimerMs > timerMs ){
      if ( func == nullptr )
        //switchOn();
        onTimer();
      else
        func( argc, argv);

      ret = true;
      resetTimer();
    }  
    return ret;
  };
*/
 
};
