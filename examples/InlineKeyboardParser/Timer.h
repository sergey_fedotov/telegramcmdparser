#pragma once

#ifndef DEBUG_PRINT_
#define DEBUG_PRINT_(x)
#endif

class TimerOne {
  private:
  unsigned long timeout;
  unsigned int start = 0;
  bool status = false;
  
  public:
  TimerOne(unsigned long timeout):
  timeout(timeout)
  {};
  ~TimerOne(){};
  void set(unsigned long period){
    timeout = period;
  };
  inline unsigned long period() const { return timeout; }
  virtual void begin(){
    status = true;
    start = millis();
  };
  unsigned long lastMs() const {
    return status ? timeout - (millis()-start) : -1 ;
  };
  inline bool protectedPeriod(unsigned long period) const {
    if ( ! status ) return status;
    return millis() - start > timeout - period;
  };
  bool loop(){
    if ( !status ) return status;
    bool ret = (millis()-start) > timeout;
    if ( ret ) onTimer();
    return ret; 
  };
  inline void setStatus(bool st = true){ status = st; };
  virtual void onTimer() { status = false; };
};
