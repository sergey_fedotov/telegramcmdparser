#pragma once

#include <FS.h>
#include <LittleFS.h>

#ifndef DEBUG_PRINT_
#define DEBUG_PRINT_(x)
#endif

void initFS(bool force = false){
  if ( force ) LittleFS.format();
  while (!LittleFS.begin()) {
    LittleFS.format();
  }
};

class ChatMsgId {
  private:
  char* chatId = nullptr;
  
  //static constexpr char fileLastId[] PROGMEM = "chatMsgId.txt";
  unsigned long id;

  String getFileName() const {

    String name;
    if ( this->chatId != nullptr ) {
      name += this->chatId;
      name += F(".id");      
    }
 //   if( name.isEmpty() ) name += fileLastId;
 //   DEBUG_PRINT_( name );
    return name;
  };
  void init(const char * Id ){
    const int size = strlen(Id)+1;
    this->chatId = (char*)malloc( size );
    strncpy(this->chatId, Id, size);
  //  if( Id[0] == '-' ) this->chatId[0] = '_';
  //  DEBUG_PRINT_( this->chatId );
  };
  public:
  ChatMsgId( const char * Id){
    init ( Id ); 
  };
  ChatMsgId( const String& Id){
    init( Id.c_str() );
  };

   ~ChatMsgId(){
     if ( this->chatId != nullptr) free ( this->chatId );
     DEBUG_PRINT_( "ChatMsgId deleted");
   };
  
  inline unsigned long get() const
    { return id; };
  inline void set(unsigned long id) {
    this->id = id;
  };

  bool save(unsigned long id = 0){  
    String fileName = getFileName();
    File f = LittleFS.open( fileName.c_str(),"w");
    if ( !f ) {
      DEBUG_PRINT_( ( getFileName() +" Error open file" ) );
      return false;
    } 
    
    unsigned long writeId = id ? id : this->id;
    size_t writed = f.print( writeId );
    if ( writed ) {
      set(writeId);
    } 
    delay(1);
    f.close();
    DEBUG_PRINT_( ( getFileName() + F(" << id ") + writeId ) );
    return writed;
  };

  bool read(){
    String fileName = getFileName();
    DEBUG_PRINT_( fileName );
    File f = LittleFS.open( fileName.c_str(),"r");
    if ( !f ) {
      DEBUG_PRINT_( ( getFileName() +" Error open file" ) );
      return false;
    }
    String readId = f.readString();
    DEBUG_PRINT_( ( getFileName() + F(" >> id ") + readId ) );
    this->id = atoi( readId.c_str() );
    f.close();
    return this->id != 0; 
  };

  bool remove(){
    return LittleFS.remove( getFileName().c_str());
  };

};
