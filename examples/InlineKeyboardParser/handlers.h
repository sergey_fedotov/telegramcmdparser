#pragma once 
#include <UniversalTelegramBot.h>
#include <TelegramKeyboard.h>
#include "Relay.h"
#include "files.h"
#include "TelegramCommandParser.h"

#ifndef DEBUG_PRINT_
#define DEBUG_PRINT_(x)
#endif

extern const unsigned long relay_timeout;
extern UniversalTelegramBot bot;
extern Relay relay;

const char ledCmd[] PROGMEM = "/led";
const char startCmd[] PROGMEM = "/start";
const char renewCmd[] PROGMEM = "/renew";

template <typename T> inline String& operator << (String &s, T n){
  s += n; return s;
};
template <typename T> inline String operator + (const char* chars, T n){
  String s(chars); s += n; return s;
};

class MyParser : public TelegramParser {
  public:
  MyParser(const TelegramHandler * handlers): 
    TelegramParser( handlers ) 
  {};
  virtual bool unknownCommand( const TelegramMessage* msg = nullptr) const {    
    bool not_found = !found();
    DEBUG_PRINT_( cmdIndex() );
    if ( not_found && msg != nullptr && !msg->text.isEmpty() ) {
      String r;
      r << F("<b>Don't understand</b>: <i>") << msg->text << F("</i>");
      bot.sendMessage( msg->chat.id, r, "HTML" );      
    } 
    return not_found;
  };
};


// void inline fakeHandler(const char * args, const TelegramMessage& msg = nullptr) {
//   DEBUG_PRINT_(args);
//   DEBUG_PRINT_(msg.chat.id);
//   DEBUG_PRINT_(out);
//   bot.sendMessage( msg.chat.id, out);
// };
void inline startHelp(const char * args, const TelegramMessage& msg ) { 
  bot.sendMessage( msg.chat.id, "Help for start");
};
void inline ledHelp(const char * args, const TelegramMessage& msg ) {
  bot.sendMessage( msg.chat.id, "Help for led ");
};
void inline renewHelp(const char * args, const TelegramMessage& msg ) {
  bot.sendMessage( msg.chat.id, "Help for renewCmd");
};

const TelegramHandler handlersHelpT[] PROGMEM = {
  { startCmd+1, startHelp },
  { ledCmd+1, ledHelp },
  { renewCmd+1, renewHelp },
  {}
};
//extern MyParser parserHelp; //(handlersHelpT);



class RelayTimer : public TimerOne {
  private:
  Relay const * relay;

  public:
  RelayTimer (Relay const * relay, unsigned long period = 5000 ):
  TimerOne(period), relay(relay) 
  {};
  virtual void onTimer() { 
    setStatus( false );
    relay->switchOn();
    set(relay_timeout);
  };
  
};
extern RelayTimer timer;

bool sendControlKeyboard(const TelegramMessage& msg, const String& outMsg = ((char*)0), bool init=false);

// inline bool sendMessage(const String& replyTo, const String& text, const String& parse_mode = "", int message_id = 0){
//   return bot.sendMessage(replyTo, text, parse_mode, message_id);
// }; 
bool sendControlKeyboard(const TelegramMessage& msg, const String& outMsg , bool init){
  //DEBUG_PRINT;
  //yield();
  
  //static unsigned long keyboardMessageId = 0; 
  static String lastChatId = ((char*)0);

  static char statusTmpl[] = "Led status is ";
  
  String replayToId;
  String from_name;
  // if ( ! outMsg.isEmpty()  && !lastChatId.isEmpty() ) {
  // внутренние команды, например от таймера, отсылаем в чат с id =lastChatId от имени outMsg
   if ( ! outMsg.isEmpty()  && msg.chat.id.isEmpty() ) { 
    from_name = outMsg;
    replayToId = lastChatId;
    
  } else {
 
    replayToId = msg.chat.id;
    from_name =msg.from.name;
    lastChatId = msg.chat.id;
  
    if ( from_name.isEmpty() )
      from_name = F("Guest");
    else {
      from_name << ' '<< msg.from.lastName;
    }
    if ( !msg.from.userName.isEmpty()) {
      from_name << F(" aka(") <<  msg.from.userName << ')';
    }
  }    
  

  int ledStatus = relay.status(); //!digitalRead(LED_BUILTIN);
  TelegramKey::Inline keyLed, releyBlink;
  TelegramKey::Keyboard keys(2);
  String blinkStr;

  if ( ledStatus ) {

    keyLed = { (const char*)F("Потушить"),"led_0"};
    blinkStr << F("Потушить через ") << timer.period()/1000 << F(" сек");
    //releyBlink = { (const char*)F("Потушить через " STRINGIFY(RELAY_TIMEOUT) " сек"),(const char*)F("led")};

  } else { 

    keyLed = { (const char*)F("Зажечь"), (const char*)F("led_1") };         
    blinkStr << F("Зажечь на ") << timer.period()/1000 << F(" сек");
  }
  releyBlink = { blinkStr.c_str() ,(const char*)F("led") };
  keys << 
    keyLed << releyBlink << 
    TelegramKey::Inline{ (const char*)F("Мой канал"),(const char*)F("https://t.me/rtu5024_bot") };

  String status = statusTmpl;
  status += ledStatus ? (const char*)F("*ON*") : (const char*)F("*OFF*") ; 
  status << " by "<< from_name;
       
  ChatMsgId lastMessageInChat( replayToId );
  lastMessageInChat.read();

  // если init, то сбрасываем Id последнего сообщения в канале
  if ( init == true ) lastMessageInChat.set( 0 );              

  DEBUG_PRINT_( ( String( "msgId: ") + lastMessageInChat.get() ));
  DEBUG_PRINT_( status );        
  DEBUG_PRINT_( keys.toJson() );
  bool ret = bot.sendMessageWithInlineKeyboard(
                replayToId, 
                status, 
                "Markdown", 
                keys.toJson(),
                lastMessageInChat.get()
                ); //keyboardMessageId );
  
  // успешно отправили и нет сохраненного Id последнего сообщения
  if ( ret && lastMessageInChat.get() == 0 ){
    lastMessageInChat.save( bot.last_sent_message_id );  // сохраняем Id последнего сообщения
    DEBUG_PRINT_( (String( "Save msgId: ")  + lastMessageInChat.get() ));                   

  } else {
    if ( !ret) { 
      DEBUG_PRINT_("Error send keyboard");
      DEBUG_PRINT_(replayToId);
    }
    else if (lastMessageInChat.get() != 0) { DEBUG_PRINT_("Last message id saved already") };
  }
  return ret;

}; // sendControlKeyboard

void inline handlerRenew(const char * args, const TelegramMessage& msg ) {
  sendControlKeyboard(msg,"", true);
};
void handlerStart(const char * args, const TelegramMessage& msg ) {
   if ( strcmp_P( renewCmd+1, args) == 0 ) { 
    sendControlKeyboard(msg,"", true);
    return;
  }

  String name = msg.from.name;
  if ( name.isEmpty() )
    name = F("Guest");
  
  String response;
  response << (const char *)F("Welcome to Universal Arduino Telegram Bot library, ") << name << ".\n" <<
          (const char *)F("This is Inline Keyboard Markup example.\n\n*") <<
          renewCmd << (const char *)F("* or *") << startCmd << '_' << renewCmd+1 <<
          (const char *)F("* : returns the inline keyboard\n");
  DEBUG_PRINT_( name );
  DEBUG_PRINT_( msg.chat.id );
  DEBUG_PRINT_( response );   
   
  if ( bot.sendMessage(msg.chat.id.c_str(), response, "Markdown") ) {
    ;
  } else {
    DEBUG_PRINT_("Error");
    bot.sendMessage( msg.chat.id.c_str(), "_Error_", "Markdown");
  }
};

void handlerLed(const char * args, const TelegramMessage& msg ){
  DEBUG_PRINT_( args );  
  if ( args[0] == '1' ){
    relay.setOn();
  } else if ( args[0] == '0' ) {
    relay.setOff();
  } else  {
    relay.setOn();
    timer.begin();    
  }
  sendControlKeyboard( msg );
};
void handlerCmdLed(const char * args, const TelegramMessage& msg ){
  DEBUG_PRINT_( args );  
  if ( args == nullptr || strlen(args) == 0 ){
    relay.setOn();    
  } else {
    int timerSec = atoi( args );   
    
    if ( timerSec == 0 ){
      relay.setOff();
    } else  {
      relay.setOn();
      timer.set( timerSec * 1000 );
      timer.begin();    
    }
  }
  sendControlKeyboard( msg );
};

void handlerSetChannel(const char * args, const TelegramMessage& msg ){
  DEBUG_PRINT_( args );  
  const char* id =  &args[2];
  if ( args[0] == 'r' ){
    DEBUG_PRINT_( (String("Set report chat arg ") + id) );
    DEBUG_PRINT_( (String("Set report chat id ") + msg.chat.id) );
  } else if ( args[0] == 'c' ) {
    ChatMsgId lastMessageInChat( msg.chat.id );
    DEBUG_PRINT_( (String("Set control chat ") + id) );
    lastMessageInChat.save( bot.last_sent_message_id );     
    sendControlKeyboard( msg );
  }
  DEBUG_PRINT_( id );
};

bool timeChecker(const int h, const int m){
  return ( h > 0 && h < 24 ) && ( m >0 && m < 60);
}
void handlerHelp(const char * args, const TelegramMessage& msg ){
  DEBUG_PRINT_( args );  
  String response;
  response += "Help root";
  
  // обрабатываем доп аргументы если есть
  if ( args != nullptr && strlen(args) != 0 ){
    response += ". With args ";
    response += args;
    
    MyParser p(nullptr);
    // проверка аргумента
    const char templ[]  = "%d:%d";
    int h,m;
    int decoded = sscanf( args, templ, &h, &m );
    DEBUG_PRINT_(decoded);

    switch( decoded){
      case 1:
        response += ". Num=";
        response += h;
        break;
      case 2:
        if ( timeChecker(h,m) ){
          response += ". Time:";
          if ( h <10 ) response += '0';
          response += h;
          response += ':';
          if ( m <10 ) response += '0';
          response += m;
          break;
        }
      default:
        p.unknownCommand( &msg );
        return;
    }

  } 
  bot.sendMessage( msg.chat.id, response );
  DEBUG_PRINT_(response);
};

//const char selCallback[] PROGMEM = "set"; 

void handlerAdminChannel(const char * args, const TelegramMessage& msg) {
   DEBUG_PRINT_("Enter to chat as admin");
   String chat_id = msg.chat.id;
  TelegramKey::Keyboard keys(2);
  String set( F("set "));

  keys << 
    TelegramKey::Inline {"Журнал", (set + F("r ") + chat_id).c_str() } << 
    TelegramKey::Inline {"Управления", (set + F("c ") + chat_id).c_str() };

  bot.sendMessageWithInlineKeyboard(chat_id, "Назначить канал для", "Markdown", keys.toJson() );
};
void handlerLeftChannel(const char * args, const TelegramMessage& msg) {
  
  DEBUG_PRINT_( ( String("Left channel ") + msg.chat.id ) );

};

const TelegramHandler handlersTestT[] PROGMEM = {
  { startCmd, handlerStart },
  { renewCmd, handlerRenew }, 
  { ledCmd,   handlerCmdLed },
  {}
};
const TelegramHandler handlersRootT[] PROGMEM = { 
  { startCmd, handlerStart },
  { renewCmd, handlerRenew }, 
  { ledCmd,   handlerCmdLed },
  { "/test",  handlers:handlersTestT },
  { "/help",  handlerHelp, handlersHelpT },
  {}
};
const TelegramHandler handlersCallBackQueryT[] PROGMEM = {
  { ledCmd+1, handlerLed },
  { "set", handlerSetChannel },
  {}
};
const TelegramHandler handlersChatActions[] PROGMEM = {
  { "administrator", handlerAdminChannel },
  { "left", handlerLeftChannel },
  { "kicked", handlerLeftChannel },
  {}
};

// 445325 / 16 byte in PROGMEM
// 445341
// 445341/31212
 // const TelegramHandler testHandlers[] PROGMEM = {
 // {},
 // {}
 // };
// 445373/31228
        //445337/ 31200

//445341 / 31196

// 445345 /31200

 // 445293 / 31184