#pragma once

#ifndef DEBUG_PRINT_
  #define DEBUG_PRINT_(x)
#endif
//#define DEBUG_PRINT_(x) delay(2)

#include <Wire.h>
#include <SHT2x.h>



class MySHT : public SHT2x {
  private:
  SHT2x sht;
  float lastTemperatureF;
  float lastHumidityF;
  uint8_t present = -1;
  float module(float t){ if ( t < 0.0 ) return -t; else return t; };
  const unsigned long mtbs = 10000;
  
  public:
    enum parameter { None, Temperature, Humidity, Any };

  MySHT(const unsigned long mtbs = 10000):
    mtbs(mtbs)
  {};
  void inline autoDetect(){

    if ( isConnected() && read() ){
      uint8_t error = getError();
      DEBUG_PRINT_( error );
     
      present = !error ? 2 : -1; 
    }
//    DEBUG_PRINT_( present );
    };
   bool isMeasuringTime(){
     static unsigned int lastMeasured = 0;

     bool res =  !lastMeasured || millis() - lastMeasured > mtbs;
     if ( res ){
      lastMeasured = millis();
     }
     return res;
   };

   bool parametersChanged(){
    bool ret = false; 
    if ( !isPresent() || !isMeasuringTime() ) return ret;

    if ( ! isConnected() ) return ret;

    DEBUG_PRINT_("Connected");
    if ( ! read() ) return ret;

    DEBUG_PRINT_("Readed");
    if ( getError() != 0 ) {
      DEBUG_PRINT_((String("Error ") + getError()));
      return ret;
    }

    float temp = getTemperature();
    float hum = getHumidity();
    DEBUG_PRINT_( (String("T=") + temp));
    DEBUG_PRINT_( (String("H=") + hum));
 
    int changed = None;
 
    if ( module(lastTemperatureF - temp) > 0.50 ) {
      lastTemperatureF = temp;
      changed |= Temperature;
    }
    if ( module(lastHumidityF - hum) > 1.0 ) {
      lastHumidityF = hum;
      changed |= Humidity;
    }

    if ( changed ) onChange(changed);
     return changed;
   };

  inline int getLastTemperature(){
    return (int)(lastTemperatureF + 0.51);
  };
  uint8_t getModel(){return present; };
  
  inline bool isPresent(){
    return present >= 0;
  };
  virtual void onTemperatureChange(){};
  virtual void onHumidityChange(){};
  bool onChange(int v) {
    bool ret = false;
    if ( v & Temperature ) { onTemperatureChange(); ret = true; }
    if ( v & Humidity ) { onHumidityChange(); ret = true; }
    return ret;
  };
};
