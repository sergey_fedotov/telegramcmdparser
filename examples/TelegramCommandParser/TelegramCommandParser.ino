#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <UniversalTelegramBot.h>
#include "TelegramCommandParser.h"

// Wifi network station credentials
#define WIFI_SSID "my_ssid"
#define WIFI_PASSWORD "my_pass"
// Telegram BOT Token (Get from Botfather)
#define BOT_TOKEN "my_token"

const unsigned long BOT_MTBS = 5000; // mean time between scan messages
unsigned long bot_lasttime; 

WiFiClientSecure secured_client;
UniversalTelegramBot bot(BOT_TOKEN, secured_client);

template <typename T> inline String& operator << (String &s, T n){
  s += n; return s;
};

const char startCmd[] PROGMEM = "/start";
const char helpCmd[] PROGMEM = "/help";

class MyParser : public TelegramParser {
  public:
  MyParser(const TelegramHandler * handlers): 
    TelegramParser( handlers ) 
  {};
  virtual bool unknownCommand( const TelegramMessage* msg = nullptr) const {    
    bool res = !found();
    if ( res && msg != nullptr && !msg->text.isEmpty() ) {
      String r;
      r << F("<b>Don't understand</b>: <i>") << msg->text << F("</i>");
      bot.sendMessage( msg->chat.id, r, "HTML" );      
    } 
    return res;
  };
};

void inline handlerHelpStart(const char * args, const TelegramMessage& msg) {
  Serial.println("Run Help_Start handler");
  bot.sendMessage( msg.chat.id, "Help for start");
};

TelegramHandler handlersHelp[] PROGMEM = {
  { startCmd+1, handlerHelpStart },
  {}
};
//MyParser parserHelp( handlersHelp );


void inline handlerHelp(const char * args, const TelegramMessage& msg) {
	Serial.println("Run Help handler");
  String response;
  response += "Help root";
  bot.sendMessage( msg.chat.id, response );

};


void inline handlerStart(const char * args, const TelegramMessage& msg) {
  Serial.println("Run Start handler");
  bot.sendMessage( msg.chat.id, "Start test TelegramCommandParser");
};

TelegramHandler handlersRoot[] PROGMEM = { 
	{ startCmd, handlerStart },
	{ helpCmd,  handlerHelp, handlersHelp },
	{}
};
MyParser parserRoot( handlersRoot );

  


void setup(){
	Serial.begin(115200);
	delay(500);
  
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);

  // attempt to connect to Wifi network:
  Serial.print("Connecting to Wifi SSID ");
  Serial.print(WIFI_SSID);
  Serial.print(" ");
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  //secured_client.setTrustAnchors(&cert); // Add root certificate for api.telegram.org
  //secured_client.setInsecure(); 
  secured_client.setFingerprint( TELEGRAM_FINGERPRINT );

 
  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(500);
  }
  Serial.println();

  Serial.print("WiFi connected. IP address: ");
  Serial.println(WiFi.localIP());

  configTime(3, 0, "pool.ntp.org");
  Serial.print("Sync time ");
  time_t now = time(nullptr);
  while (now < 24 * 3600) { Serial.print(".");delay(100); now = time(nullptr); }

  Serial.println("");
  struct tm timeinfo;
  gmtime_r(&now, &timeinfo);
  Serial.print("Current time: ");
  Serial.print(asctime(&timeinfo));

  Serial.println("Test parser");
	
}

void loop(){
	if ( millis() - bot_lasttime > BOT_MTBS)
  { 

    bot_lasttime = millis();
    
    int numNewMessages = bot.getUpdates(bot.nextMessageId());
    while (numNewMessages > 0 ) // = bot.getUpdates(bot.nextMessageId()) )
    {
    	for (int i = 0; i < numNewMessages; i++)
  		{
  			int index = parserRoot.eval(bot.messages[i] );
  			String out("Parse cmd index: ");
  			if ( index < 0 ) out << "not found";
  			else  out << index;
        out << ' ' << bot.messages[i].text;
  			Serial.println( out );
        parserRoot.unknownCommand( &bot.messages[i] );

      }
      numNewMessages = bot.getUpdates(bot.nextMessageId());
    }
  }
}
